export function init() {

  const dataSource: HTMLInputElement | null = document.getElementById("data-source") as HTMLInputElement;
  const dataDestination: HTMLElement | null = document.getElementById("data-destination");

  //attach an event listener to the input element
  if(dataSource != null && dataDestination != null) {
    dataSource.addEventListener(
      'keyup',
      (e) => {
        dataDestination.innerHTML = dataSource.value
      }
    );
  }

}
