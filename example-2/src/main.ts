import { DataSource } from "./data_source";
import { BarChart } from "./bar_chart";
import { AverageCalculator } from "./average_calculator";

const input = (function(){
  const i: any = require('readline-sync');
  return {
    prompt: i.question,
    getkey: i.keyIn
  };
})();

class App {
  public static main() {

    const dataSource: DataSource = new DataSource();
    const bc1: BarChart = new BarChart();
    const ac1: AverageCalculator = new AverageCalculator();

    dataSource.addObserver(bc1);
    dataSource.addObserver(ac1);

    let userInput;

    do {
        let userInput: string = input.prompt("Do you want to change a value?");

        if(userInput === "y") {
          const idxAsStr: string = input.prompt("Index");
          const valAsStr: string = input.prompt("Value");

          const idx: number = parseInt(idxAsStr);
          const val: number = parseInt(valAsStr);

          dataSource.setValue(idx, val);

        }

        //For the purposes of the demo, we'll always
        //redraw the graph so that we can see that it hasn't
        //changed aswell as has changed.
        console.log(bc1.generate());
        console.log("The Mean Average Is:", ac1.meanAverage);

    } while(userInput != "n");
  }
}

App.main();
