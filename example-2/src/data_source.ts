import { Observable, Observer } from "./observer";

export class DataSource extends Observable {
  private data_: Array<number> = [3, 7, 2, 9];

  //Override the addObserver method to ensure that
  //the new observer is aware of the current state
  //of this object.
  public addObserver(o: Observer) {
    super.addObserver(o);

    o.notify(this);
  }

  public setValue(idx: number, value: number): void {
    if(idx >= 0 && idx < this.data_.length && value >= 0) {

      console.log("Data source is being updated...");
      this.data_[idx] = value;

      console.log("Data source is notifying all its observers");
      this.notifyAll();
    }
  }

  public get data() { return this.data_; }
}
