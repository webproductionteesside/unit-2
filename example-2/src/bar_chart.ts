import { Observer } from "./observer";
import { DataSource } from "./data_source";

export class BarChart implements Observer {

  private dataCopy: Array<number> = [];

  public notify(ds: DataSource): void {
    this.dataCopy = [];
    for(let value of ds.data) {
      this.dataCopy.push(value);
    }
  }

  public generate(): string {
    console.log("The graph...");
    let output: string = "";
    for(let value of this.dataCopy ) {
      output += value + ": " + "*".repeat(value) + "\n";
    }
    return output;
  }

}
