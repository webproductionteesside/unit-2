import { Observer } from "./observer";
import { DataSource } from "./data_source";

export class AverageCalculator implements Observer {
  private meanAverage_: number = 0;

  public notify(subject: DataSource) {
    this.meanAverage_ = 0;
    subject.data.forEach(
      (value) => {
        this.meanAverage_ += value;
      }
    );

    this.meanAverage_ /= subject.data.length;
  }

  public get meanAverage() { return this.meanAverage_; }
}
