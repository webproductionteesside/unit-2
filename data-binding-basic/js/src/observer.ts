
export abstract class Observer {
	constructor() {}

	abstract notify(subject: Observable): void;
}

export abstract class Observable {
	private observers: Array<Observer> = new Array<Observer>();

	constructor() {}

	observedBy(o: Observer): boolean {
		return this.observers.indexOf(o) !== -1;
	}

	addObserver(o: Observer): void {
		if(!this.observedBy(o)) {
			this.observers.push(o);
		}
	}

	notifyAll(): void {
		for(const o of this.observers) {
			o.notify(this);
		}
	}
}
