import { Observer, Observable } from "./observer";
export { Observer, Observable };

export class HTMLElementWrapper extends Observable
                                  implements Observer {

  private elem_: HTMLElement | HTMLInputElement;
  private isInputElement: boolean = false;

  constructor(domEl: string | HTMLElement) {
    super();

  }

  notify(subject: HTMLElementWrapper): void {

  }

  set value(v: string) {

  }

  get value(): string {

  }

  get elem(): HTMLElement | HTMLInputElement { return this.elem_; }

}
