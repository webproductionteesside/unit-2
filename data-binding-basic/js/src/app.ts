import { HTMLElementWrapper } from "./html_element_wrapper";

export function main(): void {
  var myInput = new HTMLElementWrapper('pf-001');
  var myWriter = new HTMLElementWrapper('pf-002');
  var myOtherWriter = new HTMLElementWrapper('pf-003');

  myInput.addObserver(myWriter);
  myInput.addObserver(myOtherWriter);
}
