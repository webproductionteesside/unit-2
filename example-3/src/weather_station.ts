import { Observer, Observable } from './observer';

//TODO Step 1: Complete this class

export enum EWeather { SUNNY, OVERCAST, RAINY, SNOWY };

export class WeatherStation extends /* TODO Step 1.1: extends what? */ {

	currentWeather: EWeather;

  constructor() {
    // TODO Step 1.2: Complete this constructor so that it initialises the
    // WeatherStation instance to SUNNY (its default state).
    //
    // NOTE:
    // You will need to ensure that the parent class' constructor is called.
  }

	set(w: EWeather): void {
    // TODO Step 1.3: Complete this method to change the current weather
    // conditions being monitored by the weather station.
    //
    // If the conditions have changed, then all the observers should be
    // notified.
	}

	get(): EWeather {
		return this.currentWeather;
	}
}

//TODO Step 1.4: Comment this class and its methods.
