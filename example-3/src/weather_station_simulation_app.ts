import { WeatherStation, EWeather } from "./weather_station";
import { WeatherWatcher } from "./weather_watcher";

//TODO Step 3: Complete this class, that is the driver code for the application.

export class WeatherStationSimulationApp {
	static main(): void {
		console.log("Running Weather App");

    //TODO Step 3.1: Create an instance of the WeatherStation class

    //TODO Step 3.2: Create two (or more, your choice) WeatherWatcher instances.
    //               Give them different names so you can tell which none
    //               is currently being notified by the WeatherStation object
    //               of changes in the weather.


    //TODO Step 3.3: Configure the observers/observable relationship and test
    //               that the observers are notified of the changes in weather.

	}
}

WeatherStationSimulationApp.main();

//TODO Step 3.4: Comment this class and its methods.
