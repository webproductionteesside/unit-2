export interface Observer {
	notify(subject: Observable): void;
}

export abstract class Observable {
	private observers: Array<Observer> = new Array<Observer>();

	constructor() {}

	observedBy(o: Observer): boolean {
		return this.observers.indexOf(o) !== -1;
	}

	addObserver(o: Observer): void {
		if(!this.observedBy(o)) {
			this.observers.push(o);
		}
	}

	notifyAll(): void {
		for(const o of this.observers) {
			o.notify(this);
		}
	}
}

// TODO Step 1.1: Comment this class and its methods
//                (although it should be from an earlier exercise!).
