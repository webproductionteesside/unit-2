import { WeatherStation, EWeather } from "./weather_station";
import { Observer } from "./observer";

// TODO Step 2: Complete this class

export class WeatherWatcher /* TODO Step 2.1 implements or extends */ Observer {

	private lastReport : EWeather | null = null;

	// TODO Step 2.2: Define a constructor to accept a "String" that
  //                is the name of the object.

  // TODO Step 2.3: Define the notify() method that is expected of an observer
  //                object - refer to the UML.
  // NOTE: Something to keep in mind is that the UML specifies
  //       that the single method parameter is of type 'Observable'.  The
  //       WeatherStation is-a type of Observable.
  //       As such, you can either:
  //       1) Specify the parameter to be of type WeatherStation, or
  //       2) Specify the parameter to be of type Observable, but you'll
  //          need to convert it to a WeatherStation when you need to treat
  //          it as a WeatherStation.  TypeScript provides the as keyword to
  //          achieve this 'casting' from the generic type to the specific type.


	getName(): String {
		return this.name;
	}

	getLastReport() : EWeather | null {
		return this.lastReport;
	}

};

// TODO Step 2.4: Comment this class and its methods.
