import { Model } from "./model";

export class Person extends Model {

  constructor(name: string, age: number = 10) {
    super();

    this.addAttribute('name', name);
    this.addAttribute('age', age);
  }

}
