import { Observable, Observer } from './observer';
import { HTMLElementWrapper } from './html_element_wrapper';

export class LiveValue      extends Observable
                            implements Observer {

  private changed_: boolean = false;

  constructor(private value_: any) {
    super();
  }

  get value(): any { return this.value_; }

  get changed(): boolean { return this.changed_; }

  set value(newValue: any) {

  }
  set changed(isChanged: boolean) { this.changed_ = isChanged; }

  notify(o: HTMLElementWrapper): void {

  }
}
