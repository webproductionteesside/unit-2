import { Observer, Observable } from "./observer";
import { LiveValue, Model } from "./model";
export { Observer, Observable };

export class HTMLElementWrapper   extends Observable
                                  implements Observer {

  private elem_: HTMLElement | HTMLInputElement;
  private isInputElement: boolean = false;

  constructor(domEl: string | HTMLElement) {
    super();

  }

  notify(subject: LiveValue): void {

  }

  set value(v: string) {

  }

  get value(): string {

  }

  get elem(): HTMLElement | HTMLInputElement { return this.elem_; }

}
