import { Person } from "./person_model";
import { HTMLElementWrapper } from "./html_element_wrapper";

//NOTE:
//
// myPerson is being exported so that we can change it via
// the Browser's JavaScript console.
//
// To access myPerson from the console we'd need to do the following:
//
//    const p = requirejs.s.contexts._.defined["dist/app"].myPerson;
// 
// The we can use p to change things in the object:
//
//    p.set("age", 999);
//    p.set("name", "King Henry VII");
//
// This is not recommended generally.
//
export const myPerson: Person = new Person("Joe Bloggs");

export function main() {

    //const myPerson: Person = new Person("Joe Bloggs");
    const myInput: HTMLElementWrapper = new HTMLElementWrapper('pf-001');
    const myWriter: HTMLElementWrapper = new HTMLElementWrapper('pf-002');
    const myOtherWriter: HTMLElementWrapper = new HTMLElementWrapper('pf-003');
    const myAge: HTMLElementWrapper = new HTMLElementWrapper('pf-004');
    const myOtherAge: HTMLElementWrapper = new HTMLElementWrapper('pf-005');
    const myOtherOtherAge: HTMLElementWrapper = new HTMLElementWrapper('pf-007');

    myPerson.addObservable('name', myInput);

    myPerson.addObserver('name', myInput);
    myPerson.addObserver('name', myWriter);
    myPerson.addObserver('name', myOtherWriter);
    myPerson.addObserver('age', myAge);
    myPerson.addObserver('age', myOtherAge);

    myPerson.addObserver('age', myOtherOtherAge);

    console.log(myPerson);

    myAge.elem.addEventListener(
        'click',
        (e) => {
            const currentAge = myPerson.get('age');
            myPerson.set('age', currentAge + 1);
        }
    );

    myOtherOtherAge.elem.addEventListener(
        'click',
        (e) => {
            const currentAge = myPerson.get('age');
            myPerson.set('age', currentAge + 1);
        }
    );

    myPerson.set('name', 'Steven Mead');

    const resetButton: HTMLButtonElement | null = document.getElementById('button') as HTMLButtonElement;

    if(resetButton != null) {
        resetButton.onclick = (e) => { myPerson.set('name', 'Joe Bloggs') };
    }
}
