import { Observer, Observable } from './observer';
import { LiveValue } from './live_value';

export { Observer, Observable, LiveValue };

export abstract class Model {

  protected attributes: { [name: string]: LiveValue } = {};

  get(attr: string): any {

  }

  set(attr: string, value: any): void {

  }

  protected addAttribute(name: string, value: any): void {

  }

  addObserver(attr: string, observer: Observer): void {

  }

  addObservable(attr: string, observable: Observable): void {

  }

}
